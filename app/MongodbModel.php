<?php

namespace App;

use MongoDB;
use App\Contracts\Models\CrudInterface;

abstract class MongodbModel implements CrudInterface
{
	protected $client;

	protected $collection;
	
	public function __construct()
	{
		$this->client = new MongoDB\Client(
			'mongodb+srv://webjump:kjZ49PWgECx3XbY@desafiostore-ifoyb.gcp.mongodb.net/test?retryWrites=true'
		);

		if (!isset($this->collectionName)) {
			throw new \Exception('$collectionName has not defined in ' . get_class($this) . ' model');
		}

		$this->collection = $this->client->selectCollection('Store', $this->collectionName);
	}

	/**
	 * Create one or more items
	 *
	 * @param array $data
	 *
	 * @return \MongoDB\InsertOneResult
	 */
	public function create(array $data) 
	{
		return $this->collection->insertOne($data);
	}

	/**
	 * Find one or more documents from collection
	 *
	 * @param string $id
	 *
	 * @return \MongoDB\Driver\Cursor|MongoDB\Model\BSONDocument|null
	 */
	public function read(string $id = null)
	{
		if ($id) {
			return $this->collection->findOne(['_id' => new MongoDB\BSON\ObjectID($id)]);
		} else {
			return $this->collection->find([]);
		}
	}

	/**
	 * Find or create
	 *
	 * @param array $data
	 *
	 * @return \MongoDB\Driver\Cursor|array|object|null
	 */
	public function findOrCreate(array $data)
	{
		$cursor = $this->collection->updateOne($data, [
			'$setOnInsert' => $data
		], [
			'upsert' => true
		]);

		return $cursor;
	}

	/**
	 * Update document
	 *
	 * @param array $data
	 * @param string $id
	 *
	 * @return \MongoDB\UpdateResult
	 */
	public function update(array $data, string $id) : MongoDB\UpdateResult
	{
		return $this->collection->replaceOne(['_id' => new MongoDB\BSON\ObjectID($id)], $data);
	}

	/**
	 * Delete document from collection
	 *
	 * @param string $id
	 *
	 * @return \MongoDB\DeleteResult
	 */
	public function delete(string $id) : MongoDB\DeleteResult
	{
		return $this->collection->deleteOne(['_id' => new MongoDB\BSON\ObjectID($id)]);
	}

	/**
	 * Count total documents
	 *
	 * @param string $collectionName
	 * @return integer
	 */
	public function count(): int
	{
		$command = new MongoDB\Driver\Command(["count" => $this->collectionName]);

		try {
			$result = current($this->client->getManager()->executeCommand("Store", $command)->toArray());
			$count = $result->n;

			return $count;
		} catch (MongoDB\Driver\Exception\Exception $e) {
			echo $e->getMessage(), "\n";
		}
	}

	public function getClient()
	{
		return $this->client;
	}

	public function getCollection()
	{
		return $this->collection;
	}

	public function createIndex(array $keys, array $options = array())
	{
		return $this->collection->createIndex($keys, $options);
	}
}