<?php

namespace App;

use MongoDB;

class Category extends MongodbModel
{
	protected $collectionName = 'Categories';

	protected $fillable = ['nome', 'codigo'];

	/**
	 * Checks if a category has at least 1 linked product.
	 *
	 * @param string $id
	 *
	 * @return boolean
	 */
	public function hasProduct(string $id = null)
	{
		$cursor = (new Product())->findByCategory(new MongoDB\BSON\ObjectID($id));

		return count($cursor->toArray()) > 0 ? true : false;
	}

	public function byProduct($product)
	{
		return $this->collection->find(['_id' => [
			'$in' => $product->categorias
		]]);
	}
}