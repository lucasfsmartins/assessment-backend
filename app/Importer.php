<?php

namespace App;

use App\Category;
use App\Product;
use App\Contracts\Handlers\ImportFileHandlerInterface;
use App\Exceptions\FileNotFoundException;
use MongoDB;

class Importer
{
	private static $instance = null;

	/**
	 * File Handler
	 *
	 * @var \App\Contracts\Handlers\ImportFileHandlerInterface
	 */
	protected static $handler = null;

	/**
	 * Command-line argument vector
	 *
	 * @var array
	 */
	private static $argv = [];

	/**
	 * Command-line argument count
	 *
	 * @var integer
	 */
	private static $argc = 0;

	/**
	 * File path of a file to import
	 *
	 * @var string|null
	 */
	private static $filePath = null;

	 /**
	  * Performs import
	  *
	  * @param string|null $filePath The path of file to import (this will overwrite a previously defined file path)
	  * @param array $argv
	  *
	  * @throws \InvalidArgumentException
	  * 
	  * @return string
	  */
	public function import(?string $filePath = null, array $argv = array()): string
	{
		if (!self::$handler) {
			throw new \Exception('No handler was defined. Please, use Importer::setHandler() to define a handler for execute this statically');
		}

		if ($filePath) {
			self::$handler->setFilepath($filePath);
		}

		if ($argv) {
			// Parses arguments passed by CLI as --set-delimiter=',' to setDelimiter()
			self::parsesArguments($argv, self::$handler);
		}

		$items = array_slice(self::$handler->getItemsArray(), 20, 25);

		if ($items) {
			$productModel = new Product;

			$manager = $productModel->getClient()->getManager();
			
			$bulkProducts = new MongoDB\Driver\BulkWrite(['ordered' => true]);

			$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);

			$categoryModel = new Category;
			$categories = $categoryModel->read();

			$categoriasArray = [];

			$newCategoriesCount = 0;

			foreach ($categories as $category) {
				$categoriesArray["{$category->_id}"] = $category->nome;
			}

			foreach ($items as $item) {
				if ($item['categoria']) {
					$categorias = explode('|', $item['categoria']);

					if ($categorias) {
						foreach ($categorias as $categoria) {
							if ($categoria_id = array_search($categoria, $categoriesArray)) {
								$item['categorias'][] = new MongoDB\BSON\ObjectID($categoria_id);
							} else {
								$resultNewCategory = $categoryModel->create([
									'nome' => $categoria
								]);

								$newCategoryId = (string) $resultNewCategory->getInsertedId();

								// Update categoriesArray with new category for future checks
								$categoriesArray["{$newCategoryId}"] = $categoria;

								// Update categories for this new product
								$item['categorias'][] = $resultNewCategory->getInsertedId();

								$newCategoriesCount++;
							}
						}
					}

					unset($item['categoria']);
				}

				if ($item['quantidade']) {
					$item['quantidade'] = (int) $item['quantidade'];
				}

				if ($item['preco']) {
					$item['preco'] = (float) $item['preco'];
				}

				$bulkProducts->insert($item);
			}

			try {
				$resultProducts = $manager->executeBulkWrite('Store.Products', $bulkProducts, $writeConcern);
			} catch (MongoDB\Driver\Exception\BulkWriteException $e) {
				$resultProducts = $e->getWriteResult();

				// Check if the write concern could not be fulfilled
				if ($writeConcernError = $resultProducts->getWriteConcernError()) {
					printf("%s (%d): %s\n",
						$writeConcernError->getMessage(),
						$writeConcernError->getCode(),
						var_export($writeConcernError->getInfo(), true)
					);
				}

				// Check if any write operations did not complete at all
				foreach ($resultProducts->getWriteErrors() as $writeError) {
					printf("Operation#%d: %s (%d)\n",
						$writeError->getIndex(),
						$writeError->getMessage(),
						$writeError->getCode()
					);
				}
			} catch (MongoDB\Driver\Exception\Exception $e) {
				printf("Other error: %s\n", $e->getMessage());
				exit;
			}

			printf("Inserted %d category(ies)\n", $newCategoriesCount);

			printf("Inserted %d product(s)\n", $resultProducts->getInsertedCount());
			printf("Updated  %d product(s)\n", $resultProducts->getModifiedCount());
		}

		return 'Data imported successfully!';
	}

	/**
	 * Set file handler
	 *
	 * @param \App\Contracts\Handlers\ImportFileHandlerInterface $handler
	 *
	 * @return self
	 */
	public static function setHandler(ImportFileHandlerInterface $handler): self
	{
		self::$handler = $handler;

		if (self::$instance === null) {
			self::$instance = new self;
		}

		return self::$instance;
	}


	/**
	 * Get valid file path
	 *
	 * @return string|null
	 */
	public function getFilepath(): ?string
	{
		$filePath = null;

		if (self::$filePath) {
			$filePath = self::$filePath;
		} elseif (self::$argv && is_string(self::$argv[1])) {
			if (file_exists(self::$argv[1])) {
				$filePath = self::$argv[1];
			}
		}

		return $filePath;
	}

	/**
	 * Set file path
	 *
	 * @param string $filePath
	 *
	 * @return self
	 */
	public function setFilepath(string $filePath): self
	{
		self::$filePath = $filePath;

		return $this;
	}

	/**
	 * Set Command-line arguments
	 *
	 * @param array $argv
	 * @param integer $argc
	 *
	 * @return self
	 */
	public function setCliArguments(array $argv, int $argc = 0): self
	{
		self::$argv = $argv;
		self::$argc = $argc;

		return $this;
	}

	/**
	 * Parses passed by CLI as --set-delimiter=',' and --set-qualifier=';' 
	 * to call handler methods such as setDelimiter() and setQualifier(), respectively.
	 *
	 * @param array $argv
	 *
	 * @return self
	 */
	public function parsesCliArguments(array $argv = array()): self
	{
		if (!self::$handler) {
			throw new \Exception('No handler was defined. Please, use Importer::setHandler() to define a handler for execute this statically');
		}

		$arguments = $argv ?: self::$argv;
		
		if ($arguments) {
			foreach ($arguments as $arg) {
				if (\strstr($arg, '--') && $attribute = \strstr($arg, '=', true)) {
					$methodName = \lcfirst(\str_replace('-', '', \ucwords($attribute, '-')));
					$value = (string) \explode('=', $arg, 2)[1];

					if (method_exists(self::$handler, $methodName)) {
						self::$handler->$methodName($value);
					} else {
						throw new \InvalidArgumentException("'$arg' is not recognized as a qualified argument for this operation");
					}
				}
			}
		} else {
			throw new \Exception('No argv was defined. Please, use Importer::setCliArguments() to define a handler for this operation');
		}

		return $this;
	}
}