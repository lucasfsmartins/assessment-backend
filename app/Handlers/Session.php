<?php

namespace App\Handlers;

/**
 * Handle session
 */
class Session
{
	/**
	 * Session has started
	 */
	const STARTED = true;

	/**
	 * Session not started
	 */
	const NOT_STARTED = false;

	/**
	 * @var boolean
	 */
	private $sessionState = self::NOT_STARTED;

	private static $instance;

	/**
	 * Get instance of Session class.
	 *
	 * If the session was not started previously, it is automatically started.
	 *
	 * @return self
	 */
	public static function getInstance(): self
	{
		if (!isset(self::$instance)) {
			self::$instance = new self;
		}

		self::$instance->startSession();

		return self::$instance;
	}

	/**
	 * Start session
	 *
	 * @param array $options If provided, this is an associative array of options that will override the currently set session configuration directives.
	 * 
	 * @see http://php.net/manual/pt_BR/function.session-start.php
	 * @see http://php.net/manual/pt_BR/session.configuration.php
	 *
	 * @return boolean
	 */
	public function startSession(array $options = array()): bool
	{
		if ($this->sessionState === self::NOT_STARTED) {
			$this->sessionState = session_start($options);
		}

		return $this->sessionState;
	}

	/**
	 * Stores datas in the session.
	 *
	 * @param mixed $name
	 * @param mixed $value
	 *
	 * @return void
	 */
	public function __set($name , $value): void
	{
		$_SESSION[$name] = $value;
	}

	/**
	 * Gets datas from the session.
	 * 
	 * @example $instance->foo
	 *
	 * @param mixed $name
	 *
	 * @return mixed
	 */
	public function __get($name)
	{
		if (isset($_SESSION[$name])) {
			return $_SESSION[$name];
		}
	}

	/**
	 * Is set in session?
	 *
	 * @param mixed $name
	 *
	 * @return boolean
	 */
	public function __isset($name): bool
	{
		return isset($_SESSION[$name]);
	}

	public function __unset($name): void
	{
		unset($_SESSION[$name]);
	}

	/**
	 * Destroys the current session
	 *
	 * @return boolean
	 */
	public function destroy(): bool
	{
		if ($this->sessionState === self::STARTED) {
			$this->sessionState = session_destroy();

			$_SESSION = array();

			return $this->sessionState;
		}

		return false;
	}
}
