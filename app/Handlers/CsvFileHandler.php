<?php

namespace App\Handlers;

use App\Contracts\Handlers\ImportFileHandlerInterface;

class CsvFileHandler implements ImportFileHandlerInterface
{
	private $filePath = null;

	private $headers = null;

	private $fileResource = null;

	protected $delimiter = ',';

	protected $qualifier = '"';

	/**
	 * Undocumented function
	 *
	 * @param string|null $filePath
	 * @param array|null|boolean $headers
	 */
	public function __construct($filePath = null, $headers = null)
	{
		if ($filePath !== null) {
			$this->setFilepath($filePath);
		}

		$this->headers = $headers;
	}

	private function openFile()
	{
		if ($this->filePath === null) {
			throw new \Exception("filePath is not set");
		}

		if (!file_exists($this->filePath)) {
			throw new \Exception(sprintf('File "%s" is not found', $this->filePath));
		}

		/**
		 * @todo for MAC, need test
		 */
		//var_dump(ini_get('auto_detect_line_endings')); die('EOF auto_detect_line_endings'); 

		$this->fileResource = fopen($this->filePath, 'r');

		if ($this->fileResource === false) {
			throw new \Exception('The file could not be opened');
		}
	}

	private function closeFile()
	{
		if (is_resource($this->fileResource)) {
			if (!fclose($this->fileResource)) {
				throw new Exception("The file resource could not be closed");
			}
		}

		$this->fileResource = null;
	}

	/**
	 * Set file path
	 *
	 * @param string $filePath
	 *
	 * @return void
	 */
	public function setFilepath(string $filePath)
	{
		$this->filePath = $filePath;
		$this->headers = null;

		if ($this->fileResource) {
			fclose($this->fileResource);
			$this->fileResource = null;
		}
	}

	public function getItems()
	{
		$this->openFile();

		$headers = $this->headers;

		$countHeaders = 0;

		if ($headers !== false) {
			if ($headers === null) {
				$cols = \fgetcsv($this->fileResource, 0, $this->delimiter, $this->qualifier);
				$headers = $cols;
			}

			$countHeaders = \count($headers);
		}

		while (
			$this->fileResource 
			&& !\feof($this->fileResource) 
			&& ($cols = \fgetcsv($this->fileResource, 0, $this->delimiter, $this->qualifier)) !== false
		) {
			if ($headers === false) {
				$item = $cols;
			} else {
				$countCols = \count($cols);

				if ($countHeaders < $countCols) {
					$item = $countHeaders ? \array_combine($headers, \array_slice($cols, 0, $countHeaders)) : [];
				} elseif ($countHeaders > $countCols) {
					$item = \array_combine(array_slice($headers, 0, $countCols), $cols);

					for ($i = $countCols; $i < $countHeaders; $i++) {
						$item[$i] = null;
					}
				} else {
					$item = \array_combine($headers, $cols);
				}
			}

			yield $item;
		}

		$this->closeFile();
	}

	public function getItemsArray()
	{
		$result = [];

		$items = $this->getItems();

		foreach ($items as $item) {
			$result[] = $item;
		}

		return $result;
	}

	public function setHeaders(?array $headers)
	{
		$this->headers = $headers;
	}

	public function setDelimiter(string $delimiter)
	{
		$this->delimiter = $delimiter;
	}

	public function setQualifier(string $qualifier)
	{
		$this->qualifier = $qualifier;
	}

	public function countItems(): int
	{
		$count = 0;

		$this->openFile();

		if ($this->headers === null) {
			\fgetcsv($this->fileResource, 0, $this->delimiter, $this->qualifier);
		}

		while ($this->fileResource && !\feof($this->fileResource)) {
			$cols = \fgetcsv($this->fileResource, 0, $this->delimiter, $this->qualifier);

			if (empty($cols)) {
				continue;
			}

			$count++;
		}

		$this->closeFile();

		return $count;
	}

	public function __deconstruct()
	{
		if ($this->fileResource) {
			$this->closeFile();
		}
	}
}