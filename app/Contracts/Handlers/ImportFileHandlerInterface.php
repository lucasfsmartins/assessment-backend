<?php

namespace App\Contracts\Handlers;

interface ImportFileHandlerInterface
{
	public function setFilepath(string $filePath);

	public function getItems();

	public function getItemsArray();

	public function countItems(): int;
}