<?php

namespace App\Contracts\Models;

interface CrudInterface
{
    public function create(array $data);
    public function read(string $id = null);
    public function update(array $data, string $id);
    public function delete(string $id);
}