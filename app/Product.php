<?php

namespace App;

use MongoDB;

class Product extends MongodbModel
{
	protected $collectionName = 'Products';

	protected $fillable = ['nome', 'sku', 'preco', 'quantidade', 'categorias'];

	/**
	 * Create one or more items
	 *
	 * @param array $data
	 *
	 * @return \MongoDB\InsertOneResult
	 */
	public function create(array $data) 
	{
		if ($data['categorias']) {
			foreach ($data['categorias'] as $key => $category_id) {
				if (!$category_id instanceof MongoDB\BSON\ObjectID) {
					$data['categorias'][$key] = new MongoDB\BSON\ObjectID($category_id);
				}
			}
		}

		return parent::create($data);
	}

	/**
	 * Find products by category
	 *
	 * @param MongoDB\BSON\ObjectID $category
	 *
	 * @return MongoDB\Driver\Cursor
	 */
	public function findByCategory(MongoDB\BSON\ObjectID $category)
	{
		return $this->collection->find(['categorias' => $category]);
	}

	public function getCategoriesByProduct($product)
	{
		$categoryModel = new Category;

		var_dump($categoryModel->read()); die;
	}

	/**
	 * Update document
	 *
	 * @param array $data
	 * @param string $id
	 *
	 * @return \MongoDB\UpdateResult
	 */
	public function update(array $data, string $id) : MongoDB\UpdateResult
	{
		if ($data['categorias']) {
			foreach ($data['categorias'] as $key => $category_id) {
				if (!$category_id instanceof MongoDB\BSON\ObjectID) {
					$data['categorias'][$key] = new MongoDB\BSON\ObjectID($category_id);
				}
			}
		}
		
		return parent::update($data, $id);
	}

	public function getPreco()
	{
		return $this->preco;
	}
}