<?php

namespace App\Controllers;

use App\View;
use App\Product;
use App\Validator;
use App\Handlers\Session;
use App\ImageUploader;
use App\Category;

class ProductController
{
	static $requiredFields = ['sku', 'nome', 'preco', 'quantidade'];

	public function index()
	{
		$productModel = new Product;
		$productModel->createIndex(['categorias' => 1]);

		$products = $productModel->read();

		$categoryModel = new Category;

		(new View('products'))
			->assign('products', $products)
			->assign('categoryModel', $categoryModel);
	}

	public function create()
	{
		$categoryModel = new \App\Category;
		$categories = $categoryModel->read();

		(new View('addProduct'))
			->assign('categories', $categories);
	}

	public function store()
	{
		$data = input()->all();

		$image = input()->file('foto');

		$result = Session::getInstance();
		
		if ($image->getMime() === 'image/jpeg') {
			$destinationFilename = sprintf('%s.%s', uniqid('f', false) . '_' . str_shuffle(implode(range('f', 'l'))), $image->getExtension());
			
			if ($image->move(APP_ROOT_PATH . sprintf('/public/uploads/%s', $destinationFilename))) {
				$data['foto'] = $destinationFilename;
			} else {
				$result->errors = 'Ocorreu um erro ao tentar enviar a foto.' . ($image->errors ? 'Erro: ' . $image->errors : '');
				$result->data = $data;

				redirect('/products/new');
			}
		}
		
		// Parse preco to float
		if ($data['preco']) {
			$data['preco'] = tofloat($data['preco']);
		}

		$validator = new Validator($data);
		$validator->rule('required', self::$requiredFields);
		$validator->rule('integer', 'quantidade');

		if ($validator->validate()) {
			$product = new Product;
			$product->create($data);

			$result->message = 'Product added successfully!';
		} else {
			$result->errors = $validator->errors();
			$result->data = $data;

			redirect('/products/new');
		}

		redirect('/products');
	}

	public function edit($id)
	{
		$productModel = new Product;
		$product = $productModel->read($id);

		if ($product->preco) {
			$product->preco = number_format($product->preco, 2, ',', '.');
		}

		$categoryModel = new \App\Category;
		$categories = $categoryModel->read();

		(new View('addProduct'))
			->assign('product', $product)
			->assign('categories', $categories);
	}

	public function update($id)
	{
		$data = input()->all();

		// Parse preco to float
		if ($data['preco']) {
			$data['preco'] = tofloat($data['preco']);
		}

		$validator = new Validator($data);
		$validator->rule('required', self::$requiredFields);
		
		$result = Session::getInstance();
		
		if ($validator->validate()) {
			$product = new Product;
			$product->update($data, $id);

			$result->message = 'Product updated successfully!';
		} else {
			$result->errors = $validator->errors();
			
			redirect('/products/' . $id . '/edit');
		}

		redirect('/products');
	}

	public function delete($id)
	{
		$productModel = new Product;
		$deleteResult = $productModel->delete($id);

		$result = Session::getInstance();

		if ($deleteResult->isAcknowledged() && $deleteResult->getDeletedCount() == 1) {
			$result->message = 'Product deleted successfully!';
		} else {
			$result->errors = 'An error occurred while trying to delete the product.';
		}

		redirect('/products');
	}
}