<?php

namespace App\Controllers;

use App\View;
use App\Product;

use MongoDB;

class DashboardController
{
    public function index()
    {
        $productModel = new Product;
        $products = $productModel->read();

        $countProducts = $productModel->count();

		(new View('dashboard'))
            ->assign('products', $products)
            ->assign('countProducts', $countProducts);
    }
}