<?php

namespace App\Controllers;

use App\View;
use App\Category;
use App\Validator;
use App\Handlers\Session;

class CategoryController
{
	static $requiredFields = ['nome', 'codigo'];

	public function __construct()
	{
		// TODO
	}

	public function index()
	{
		$categoryModel = new Category;
		$categories = $categoryModel->read();

		(new View('categories'))
			->assign('categories', $categories);
	}

	public function create()
	{
		new View('addCategory');
	}

	public function store()
	{
		$data = input()->all();

		$validator = new Validator($data);
		$validator->rule('required', self::$requiredFields);

		$result = Session::getInstance();

		if ($validator->validate()) {
			$category = new Category;
			$category->create($data);

			$result->message = 'Category added successfully!';
		} else {
			$result->errors = $validator->errors();
			$result->data = $data;

			redirect('/categories/new');
		}

		redirect('/categories');
	}

	public function edit(string $id)
	{
		$categoryModel = new Category;
		$category = $categoryModel->read($id);

		(new View('addCategory'))
			->assign('category', $category);
	}

	public function update(string $id)
	{
		$data = input()->all();
		
		$validator = new Validator($data);
		$validator->rule('required', self::$requiredFields);

		$result = Session::getInstance();

		if ($validator->validate()) {
			$category = new Category;
			$category->update($data, $id);

			$result->message = 'Category updated successfully!';
		} else {
			$result->errors = $validator->errors();
			
			redirect('/categories/' . $id . '/edit');
		}

		redirect('/categories');
	}

	public function delete(string $id) : void
	{
		$categoryModel = new Category;

		$result = Session::getInstance();
		
		if ($categoryModel->hasProduct($id)) {
			$result->errors = 'This category has at least one linked product.';
		} else {
			$deleteResult = $categoryModel->delete($id);

			if ($deleteResult->isAcknowledged() && $deleteResult->getDeletedCount() == 1) {
				$result->message = 'Category deleted successfully!';
			} else {
				$result->errors = 'An error occurred while trying to delete the category.';
			}
		}
		
		redirect('/categories');
	}
}