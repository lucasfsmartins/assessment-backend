<?php

namespace App;

use Exceptions\ViewNotFoundException;

class View
{
	/**
	 * @var array Data to be extracted for a view
	 */
	private $data = array();

	/**
	 * @var string|bool File to be rendered
	 */
	private $render = false;

	/**
	 * @var string Default layout
	 */
	protected $layoutDefault = 'layout';

	/**
	 * @var string Title for the page
	 */
	protected $title = '';

	/**
	 * @var bool Sets whether the title is set automatically using the last part of the URI.
	 */
	protected $setTitleAutomatically = true;

	public function __construct($template)
	{
		if ($this->setTitleAutomatically) {
			$this->setTitle(\ucfirst(\basename($_SERVER['REQUEST_URI'])));
		}

		$this->render = APP_ROOT_PATH . '/resources/views/' . $template . '.html.php';

		if (!\file_exists($this->render)) {
			throw new ViewNotFoundException("View [{$template}] not found!");
		}
	}

	public function assign($variable, $value): self
	{
		$this->data[$variable] = $value;

		return $this;
	}

	public function render()
	{
		extract($this->data);

		ob_start();
    		include($this->render);
			$content = ob_get_contents();
		ob_end_clean();
		
		include(APP_ROOT_PATH . '/resources/views/layouts/' . $this->layoutDefault . '.html.php');
	}

	public function setTitle(string $title)
	{
		$this->title = $title;
	}

	public function setTitleAutomatically(bool $trueOrFalse)
	{
		$this->setTitleAutomatically = $trueOrFalse;
	}

	public function getTitle(): string
	{
		return $this->title;
	}

	public function getTitleAutomatically(): bool
	{
		return $this->setTitleAutomatically;
	}

	public function __destruct()
	{
		$this->render();
	}
}