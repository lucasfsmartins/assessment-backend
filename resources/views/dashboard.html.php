<div class="header-list-page">
	<h1 class="title">Dashboard</h1>
</div>
<div class="infor">
	You have <?= $countProducts ?> products added on this store: <a href="/products/new" class="btn-action">Add new Product</a>
</div>
<ul class="product-list">
	<?php foreach ($products as $product) : ?>
		<li>
			<div class="product-image">
				<img src="/uploads/<?= $product->foto ?? 'nophoto.jpeg' ?>" layout="responsive" width="164" height="145" alt="<?= $product->nome ?>">
			</div>
			<div class="product-info">
				<div class="product-name"><span><?= $product->nome ?></span></div>
				<div class="product-price"><span class="special-price"><?= $product->quantidade > 0 ? $product->quantidade . ' available' : 'Out of stock' ?></span> <span>R$<?= number_format($product->preco, 2, ',', '.') ?></span></div>
			</div>
		</li>
	<?php endforeach ?>
</ul>