<?php
use App\Handlers\Session;

$storedData = Session::getInstance();
?>

<?php if ($storedData->__isset('message')) : ?>
	<div class="alert alert-info" role="alert">
		<?= $storedData->message ?>
	</div>
	<?php $storedData->__unset('message') ?>
<?php endif ?>