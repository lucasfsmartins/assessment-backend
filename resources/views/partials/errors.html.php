<?php
use App\Handlers\Session;

$storedData = Session::getInstance();
?>

<?php if ($storedData->__isset('errors')) : ?>
	<div class="alert alert-danger" role="alert">
		<?php if (is_array($errors = $storedData->errors)) : ?>
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= $error[0] ?></li>
				<?php endforeach ?>
			</ul>
		<?php else : ?>
			<?= $errors?>
		<?php endif ?>
	</div>
	<?php $storedData->__unset('errors') ?>
<?php endif ?>
