<?php
$this->setTitle(isset($product->_id) ? 'Edit' : 'New');
?>

<h1 class="title new-item"><?= isset($product->_id) ? 'Edit' : 'New' ?> Product</h1>

<?php include('partials/errors.html.php') ?>

<?php
use App\Handlers\Session;

$storedData = Session::getInstance();

if ($storedData->__isset('data')) {
	$product = (object) $storedData->data;
	$storedData->__unset('data');
}
?>

<form method="POST" action="/products/<?= isset($product->_id) ? $product->_id . '/update' : 'store' ?>" enctype="multipart/form-data">
	<div class="input-field">
		<label for="sku" class="label">Product photo</label>
		<input type="file" accept="image/jpeg" id="photo" name="foto" value="<?= $product->sku ?? null ?>" class="input-text">
	</div>
	<div class="input-field">
		<label for="sku" class="label">Product SKU</label>
		<input type="text" id="sku" name="sku" value="<?= $product->sku ?? null ?>" class="input-text">
	</div>
	<div class="input-field">
		<label for="name" class="label">Product Name</label>
		<input type="text" id="name" name="nome" value="<?= $product->nome ?? null ?>" class="input-text">
	</div>
	<div class="input-field">
		<label for="price" class="label">Price</label>
		<input type="text" id="price" name="preco" value="<?= $product->preco ?? null ?>" class="input-text">
	</div>
	<div class="input-field">
		<label for="quantity" class="label">Quantity</label>
		<input type="text" id="quantity" name="quantidade" value="<?= $product->quantidade ?? null ?>" class="input-text">
	</div>
	<div class="input-field">
		<label for="category" class="label">Categories</label>
		<select multiple id="category" name="categorias[]" class="input-text">
			<?php foreach ($categories as $category) : ?>
				<option value="<?= $category->_id ?>" <?= isset($product->_id) && isset($product->categorias) && in_array($category->_id, array_map(function($obj) { return (string) $obj; }, $product->categorias->bsonSerialize())) ? 'selected' : '' ?>><?= $category->nome ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="input-field">
		<label for="description" class="label">Description</label>
		<textarea id="description" name="descricao" class="input-text"><?= $product->descricao ?? null ?></textarea>
	</div>
	<div class="actions-form">
		<a href="/products" class="action back">Back</a>
		<input class="btn-submit btn-action" type="submit" value="Save Product">
	</div>
</form>