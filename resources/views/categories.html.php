<?php
$this->setTitle('Categories');
?>

<div class="header-list-page">
	<h1 class="title">Categories</h1>
	<a href="/categories/new" class="btn-action">Add new Category</a>
</div>

<?php include('partials/messages.html.php') ?>
<?php include('partials/errors.html.php') ?>

<table class="data-grid">
	<tr class="data-row">
		<th class="data-grid-th"><span class="data-grid-cell-content">Name</span></th>
		<th class="data-grid-th"><span class="data-grid-cell-content">Code</span></th>
		<th class="data-grid-th"><span class="data-grid-cell-content">Actions</span></th>
	</tr>
	<?php foreach ($categories as $category) : ?>
		<tr class="data-row">
			<td class="data-grid-td">
				<span class="data-grid-cell-content"><?= $category->nome ?></span>
			</td>
			<td class="data-grid-td">
				<span class="data-grid-cell-content"><?= $category->codigo ?? null ?></span>
			</td>
			<td class="data-grid-td">
				<div class="actions">
					<div class="action edit"><a href="<?= url('CategoryController@edit', ['id' => $category->_id]) ?>">Edit</a></div>
					<div class="action delete"><a href="<?= url('CategoryController@delete', ['id' => $category->_id]) ?>">Delete</a></div>
				</div>
			</td>
		</tr>
	<?php endforeach ?>
</table>