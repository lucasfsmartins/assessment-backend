<div class="header-list-page">
	<h1 class="title">Products</h1>
	<a href="/products/new" class="btn-action">Add new Product</a>
</div>

<?php include('partials/messages.html.php') ?>
<?php include('partials/errors.html.php') ?>

<table class="data-grid">
	<tr class="data-row">
		<th class="data-grid-th"><span class="data-grid-cell-content">Name</span></th>
		<th class="data-grid-th"><span class="data-grid-cell-content">SKU</span></th>
		<th class="data-grid-th"><span class="data-grid-cell-content">Price</span></th>
		<th class="data-grid-th"><span class="data-grid-cell-content">Quantity</span></th>
		<th class="data-grid-th"><span class="data-grid-cell-content">Categories</span></th>
		<th class="data-grid-th"><span class="data-grid-cell-content">Actions</span></th>
	</tr>
	<?php foreach ($products as $product) : ?>
		<tr class="data-row">
			<td class="data-grid-td">
				<span class="data-grid-cell-content"><?= $product->nome ?></span>
			</td>
			<td class="data-grid-td">
				<span class="data-grid-cell-content"><?= $product->sku ?></span>
			</td>
			<td class="data-grid-td">
				<span class="data-grid-cell-content">R$ <?= number_format($product->preco, 2, ',', '.') ?></span>
			</td>
			<td class="data-grid-td">
				<span class="data-grid-cell-content"><?= $product->quantidade ?></span>
			</td>
			<td class="data-grid-td">
				<span class="data-grid-cell-content">
					<?php 
					if (isset($product->categorias)) {
						$productCategoriasCount = $product->categorias->count();
						$categorias = $categoryModel->byProduct($product);
						
						foreach ($categorias as $key => $categoria) {
							echo $categoria->nome . (($productCategoriasCount - 1 > $key) ? '<br>' : null);
						}
					}
					?>
				</span>
			</td>
			<td class="data-grid-td">
				<div class="actions">
					<div class="action edit"><a href="<?= url('ProductController@edit', ['id' => $product->_id]) ?>">Edit</a></div>
					<div class="action delete"><a href="<?= url('ProductController@delete', ['id' => $product->_id]) ?>">Delete</a></div>
				</div>
			</td>
		</tr>
	<?php endforeach ?>
</table>