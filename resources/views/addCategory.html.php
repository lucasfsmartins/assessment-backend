<h1 class="title new-item"><?= isset($category->_id) ? 'Edit' : 'New' ?> Category</h1>

<?php include('partials/errors.html.php') ?>

<?php
use App\Handlers\Session;

$storedData = Session::getInstance();

if ($storedData->__isset('data')) {
	$category = (object) $storedData->data;
	$storedData->__unset('data');
}
?>

<form method="POST" action="/categories/<?= isset($category->_id) ? $category->_id . '/update' : 'store' ?>">
	<div class="input-field">
		<label for="category-name" class="label">Category Name</label>
		<input type="text" id="category-name" name="nome" value="<?= $category->nome ?? null ?>" class="input-text">
	</div>
	<div class="input-field">
		<label for="category-code" class="label">Category Code</label>
		<input type="text" id="category-code" name="codigo" value="<?= $category->codigo ?? null ?>" class="input-text">
	</div>
	<div class="actions-form">
		<a href="/categories" class="action back">Back</a>
		<input class="btn-submit btn-action" type="submit" value="Save">
	</div>
</form>