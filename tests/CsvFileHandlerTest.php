<?php

use App\Handlers\CsvFileHandler;
use PHPUnit\Framework\TestCase;

class CsvFileHandlerTest extends TestCase
{
    public function testGetItems()
    {
        $filepath = sys_get_temp_dir() . '/test_CsvFileHandlerTest_testGetItems_' . microtime(true) . '.txt';
        file_put_contents($filepath, implode("\r\n", [
            implode(',', ['key1', 'key2']),
            implode(',', ['r1_1', 'r1_2']),
            implode(',', ['r2_1', 'r2_2']),
        ]) . "\r\n");

        $fileHandler = new CsvFileHandler();
        $fileHandler->setFilepath($filepath);

        $actual = [];
        foreach ($fileHandler->getItems() as $item) {
            $actual[] = $item;
        }

        $expected = [
            ['key1' => 'r1_1', 'key2' => 'r1_2'],
            ['key1' => 'r2_1', 'key2' => 'r2_2'],
        ];
        $this->assertEquals($expected, $actual);

        @unlink($filepath);
    }

    public function testCountItems()
    {
        $filepath = sys_get_temp_dir() . '/test_CsvFileHandler_testCountItems_' . microtime(true) . '.txt';
        file_put_contents($filepath, implode("\r\n", [
            implode(',', ['key1', 'key2']),
            implode(',', ['r1_1', 'r1_2']),
            implode(',', ['r2_1', 'r2_2']),
        ]) . "\r\n");

        $fileHandler = new CsvFileHandler();
        $fileHandler->setFilepath($filepath);

        $actual = $fileHandler->countItems();
        $expected = 2;
        $this->assertEquals($expected, $actual);

        @unlink($filepath);
    }

    public function testGetItemsArray()
    {
        $filepath = sys_get_temp_dir() . '/test_CsvFileHandler_testGetItemsArray_' . microtime(true) . '.txt';
        file_put_contents($filepath, implode("\r\n", [
            implode(',', ['key1', 'key2']),
            implode(',', ['r1_1', 'r1_2']),
            implode(',', ['r2_1', 'r2_2']),
        ]) . "\r\n");

        $fileHandler = new CsvFileHandler();
        $fileHandler->setFilepath($filepath);

        $actual = $fileHandler->getItemsArray();

        $expected = [
            ['key1' => 'r1_1', 'key2' => 'r1_2'],
            ['key1' => 'r2_1', 'key2' => 'r2_2'],
        ];
        $this->assertEquals($expected, $actual);

        @unlink($filepath);
    }

    public function testGetItemsInitWithConstructor()
    {
        $filepath = sys_get_temp_dir() . '/test_CsvFileHandler_testGetItemsInitWithConstructor_' . microtime(true) . '.txt';
        file_put_contents($filepath, implode("\r\n", [
            implode(',', ['key1', 'key2']),
            implode(',', ['r1_1', 'r1_2']),
            implode(',', ['r2_1', 'r2_2']),
        ]) . "\r\n");

        $fileHandler = new CsvFileHandler($filepath);

        $actual = $fileHandler->getItemsArray();

        $expected = [
            ['key1' => 'r1_1', 'key2' => 'r1_2'],
            ['key1' => 'r2_1', 'key2' => 'r2_2'],
        ];

        $this->assertEquals($expected, $actual);

        @unlink($filepath);
    }

    public function testGetItemsInitWithConstructorHeaders()
    {
        $filepath = sys_get_temp_dir() . '/test_CsvFileHandler_testGetItemsInitWithConstructorHeaders_' . microtime(true) . '.txt';
        file_put_contents($filepath, implode("\r\n", [
            implode(',', ['r1_1', 'r1_2']),
            implode(',', ['r2_1', 'r2_2']),
        ]) . "\r\n");

        $fileHandler = new CsvFileHandler($filepath, ['key1', 'key2']);

        $actual = $fileHandler->getItemsArray();

        $expected = [
            ['key1' => 'r1_1', 'key2' => 'r1_2'],
            ['key1' => 'r2_1', 'key2' => 'r2_2'],
        ];

        $this->assertEquals($expected, $actual);
        

        @unlink($filepath);

    }

    public function testGetItemsInitWithFalseHeaders()
    {
        $filepath = sys_get_temp_dir() . '/test_CsvFileHandler_testGetItemsInitWithFalseHeaders_' . microtime(true) . '.txt';
        file_put_contents($filepath, implode("\r\n", [
            implode(',', ['r1_1', 'r1_2']),
            implode(',', ['r2_1', 'r2_2']),
        ]) . "\r\n");

        $fileHandler = new CsvFileHandler($filepath, false);
        
        $actual = $fileHandler->getItemsArray();

        $expected = [
            [0 => 'r1_1', 1 => 'r1_2'],
            [0 => 'r2_1', 1 => 'r2_2'],
        ];

        $this->assertEquals($expected, $actual);

        @unlink($filepath);
    }

    public function testGetItemsSwitchFile()
    {
        $filepath = sys_get_temp_dir() . '/test_CsvFileHandler_testGetItemsSwitchFile_1_' . microtime(true) . '.txt';
        file_put_contents($filepath, implode("\r\n", [
            implode(',', ['key1', 'key2']),
            implode(',', ['r1_1', 'r1_2']),
            implode(',', ['r2_1', 'r2_2']),
        ]) . "\r\n");

        $filepath2 = sys_get_temp_dir() . '/test_CsvFileHandler_testGetItemsSwitchFile_2_' . microtime(true) . '.txt';
        file_put_contents($filepath2, implode("\r\n", [
            implode(',', ['key1', 'key2']),
            implode(',', ['r1_1', 'r1_2']),
            implode(',', ['r2_1', 'r2_2']),
        ]) . "\r\n");

        $fileHandler = new CsvFileHandler();
        $fileHandler->setFilepath($filepath);

        $actual = [];

        foreach ($fileHandler->getItems() as $item) {
            $actual[] = $item;
            $fileHandler->setFilepath($filepath2); // it should stop reading current file
        }

        foreach ($fileHandler->getItems() as $item) {
            $actual[] = $item;
        }

        $expected = [
            ['key1' => 'r1_1', 'key2' => 'r1_2'],
            ['key1' => 'r1_1', 'key2' => 'r1_2'],
            ['key1' => 'r2_1', 'key2' => 'r2_2'],
        ];

        $this->assertEquals($expected, $actual);

        @unlink($filepath);
        @unlink($filepath2);
    }

    public function testGetItemsCustomDelimiters()
    {
        $filepath = sys_get_temp_dir() . '/test_CsvFileHandler_testGetItemsCustomDelimiters_' . microtime(true) . '.txt';
        file_put_contents($filepath, implode("\r\n", [
            implode(",", ['key1', 'key2']),
            implode(",", ["~r1_,\r\n1~", '~r1_2~']),
            implode(",", ['r2_1', '~r2_2~']),
        ]) . "\r\n");

        $fileHandler = new CsvFileHandler();
        $fileHandler->setFilepath($filepath);
        $fileHandler->setDelimiter(',');
        $fileHandler->setQualifier('~');

        $actual = [];
        foreach ($fileHandler->getItems() as $item) {
            $actual[] = $item;
        }

        $expected = [
            ['key1' => "r1_,\r\n1", 'key2' => 'r1_2'],
            ['key1' => 'r2_1', 'key2' => 'r2_2'],
        ];
        $this->assertEquals($expected, $actual);

        @unlink($filepath);
    }

    public function testErrorNotSetFilepath()
    {
        $this->expectException('Exception', 'Filepath is not set');

        $fileHandler = new CsvFileHandler();
        foreach ($fileHandler->getItems() as $item) {
            // no need to iterate because throws an exception
        }
    }

    public function testErrorMissingFile()
    {
        $filepath = 'not_existing';

        $this->expectException('Exception', 'File "not_existing" is not found');

        $fileHandler = new CsvFileHandler();
        $fileHandler->setFilepath($filepath);
        foreach ($fileHandler->getItems() as $item) {
            // no need to iterate because throws an exception
        }
    }
}