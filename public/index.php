<?php
/**
 * WebJump Assessment (assessment-backend)
 * 
 * @author Lucas Martins <lucasfsmartins@gmail.com>
 */

 /*
|--------------------------------------------------------------------------
| Initial settings
|--------------------------------------------------------------------------
|
*/

define('APP_ROOT_PATH', dirname(__DIR__));

ini_set('session.save_path', APP_ROOT_PATH . '/storage/app/sessions');

/*
|--------------------------------------------------------------------------
| Auto loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application.
|
*/

require_once(APP_ROOT_PATH . '/vendor/autoload.php');

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| Router class manages application routes
|
*/

require_once(APP_ROOT_PATH . '/routes.php');

require_once(APP_ROOT_PATH . '/helpers.php');

App\Router\SimpleRouter::start();