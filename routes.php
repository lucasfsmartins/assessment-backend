<?php

use App\Router\SimpleRouter;

SimpleRouter::setDefaultNamespace('App\Controllers');

SimpleRouter::get('/', 'DashboardController@index');

SimpleRouter::get('/products', 'ProductController@index');
SimpleRouter::get('/products/new', 'ProductController@create');
SimpleRouter::post('/products/store', 'ProductController@store');
SimpleRouter::get('/products/{id}/edit', 'ProductController@edit');
SimpleRouter::post('/products/{id}/update', 'ProductController@update');
SimpleRouter::get('/products/{id}/delete', 'ProductController@delete');

SimpleRouter::get('/categories', 'CategoryController@index');
SimpleRouter::get('/categories/new', 'CategoryController@create');
SimpleRouter::post('/categories/store', 'CategoryController@store');
SimpleRouter::get('/categories/{id}/edit', 'CategoryController@edit');
SimpleRouter::post('/categories/{id}/update', 'CategoryController@update');
SimpleRouter::get('/categories/{id}/delete', 'CategoryController@delete');