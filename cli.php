#!/usr/bin/php
<?php

require_once('vendor/autoload.php');

echo App\Importer::setHandler(new App\Handlers\CsvFileHandler($argv[1]))
	->setCliArguments($argv, $argc)
	->parsesCliArguments()
	->import();
?>