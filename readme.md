# WebJump Assessment - Backend
Sistema de gerenciamento de produtos.

## Features:
- Gerenciamento de produtos e estoque.

## Tecnologias:
- PHP
- MongoDB
- Composer
- Git
- Bibliotecas/dependências: php:>=7.2, ext-mongodb:^1.5.0, ext-mbstring, mongodb/mongodb:^1.4, php-di/php-di:^6.0, phpunit/phpunit:^8.0

# Instalação
1º Clone este repositório

```

git clone https://lucasfsmartins@bitbucket.org/lucasfsmartins/assessment-backend.git

```

2º Composer Install

```

composer install

```

# Run

Configure a virtualhost on your local server or simply run a php server in _public_ directory:

```

cd public
php -S localhost:8000

```

# Import products via CLI

```

php cli.php public/assets/import.csv

```

## With some arguments

```

php cli.php public/assets/import.csv --set-delimiter=';' --set-qualifier=';'

```

# Tests

To run tests execute this:

```

./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/

```